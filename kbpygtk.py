#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
try:
    import pygtk
    pygtk.require("2.0")
except:
    pass
try:
    import gtk
    import gtk.glade
except:
    sys.exit(1)
import tempfile
import os

"""http://www.pantz.org/software/shell/enhancingshellprompt.html"""
class pyMain:
    """This is an GTK Database application"""

    def __init__(self):
            
        #Set the Glade file
        self.gladefile = "kabuk.glade"  
        self.wTree = gtk.glade.XML(self.gladefile, "window1")
        dic = {"on_window1_destroy" : gtk.main_quit
                , "on_button2_clicked" : self.kaydet}
        self.wTree.signal_autoconnect(dic)
        self.combo1 = self.wTree.get_widget("combobox1")
        self.combo2 = self.wTree.get_widget("combobox2")
        self.combo3 = self.wTree.get_widget("combobox3")
        self.combo4 = self.wTree.get_widget("combobox4")
        self.combo5 = self.wTree.get_widget("combobox5")
        self.entry1 = self.wTree.get_widget("entry5")
        self.entry2 = self.wTree.get_widget("entry6")
        self.entry3 = self.wTree.get_widget("entry7")
        self.entry4 = self.wTree.get_widget("entry8")
        
    
    def get_active_text(self, combobox):
        self.combobox = combobox
        self.model = self.combobox.get_model()
        self.active = self.combobox.get_active()
        if self.active < 0:
            return None
        return self.model[self.active][0]

    
    def kaydet(self, widget):
        color_dic = { "Black" : 30
                    , "Red" : 31
                    , "Green" : 32
                    , "Brown" : 33
                    , "Blue" : 34
                    , "Purple" : 35
                    , "Cyan" : 36
                    , "Gray" : 37}
        
        self.main_color = self.get_active_text(self.combo1)
        self.current_time = self.get_active_text(self.combo2)
        self.path_color = self.get_active_text(self.combo3)
        self.command_start = self.get_active_text(self.combo4)
        
        our_string = ""
        mainfile = open("/home/%s/.bashrc" %(os.getenv("USER")), 'r')
        var1 = 0
        for i in mainfile.readlines():
            if i.find('export PS1=') != -1:
                var1 = 1
                i = 'export PS1="\[\e[%sm\][\u%s\h]\[\e[0m\]\[\e[%sm\][%s \\t]\[\e[0m\]%s\[\e[%sm\][%s \w]\[\e[%sm\]%s"\n'%(color_dic[self.main_color], self.entry1.get_text(), color_dic[self.current_time], self.entry3.get_text(), "\\n", color_dic[self.path_color], self.entry4.get_text(), color_dic[self.command_start], self.entry2.get_text())
            our_string += i
        
        if var1 == 0:
            our_string += '\nexport PS1="\[\e[%sm\][\u%s\h]\[\e[0m\]\[\e[%sm\][%s \\t]\[\e[0m\]%s\[\e[%sm\][%s \w]\[\e[%sm\]%s"\n'%(color_dic[self.main_color], self.entry1.get_text(), color_dic[self.current_time], self.entry3.get_text(), "\\n", color_dic[self.path_color], self.entry4.get_text(), color_dic[self.command_start], self.entry2.get_text())
        
        print our_string
        mainfile.close()
        mainfile = open("/home/%s/.bashrc" %(os.getenv("USER")), 'w')
        mainfile.write(our_string)
        
        if (self.get_active_text(self.combo5)) == "GNOME":
            self.subpr = os.popen2("gnome-terminal")
        else:
            self.subpr = os.popen2("konsole")
        self.entry1.delete_text(0, -1)
        self.entry2.delete_text(0, -1)
        self.entry3.delete_text(0, -1)
        self.entry4.delete_text(0, -1)
        self.combo1.set_active(-1)
        self.combo2.set_active(-1)
        self.combo3.set_active(-1)
        self.combo4.set_active(-1)
            
if __name__ == "__main__":
    app = pyMain()
    gtk.main()
